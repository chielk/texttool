texttool
========

User Interface
==============

Features:
* text with a title
* list of added texts, sorted by date added
* calculate difficulty score from the text
* searchable text
* substring search
* editable text
* adding a new text

Tools
=====

Build tool: ~~maven~~ python

Must include a database (SQL/NOSQL)


Implementation
==============

Database
--------
id, title, text, date

Difficulty should not be included in the database because it is computed from
the text and can be edited. This gives a bit more overhead because it needs to
be computed every time for each text, but could be changed if it is too slow.

API
---
For convenience, some HTML is generated from the object model. In terms of
abstraction this would normally probably be a bad choice as front-end
developers may have to change code there to change the layout. For this reason,
and for the possibility of using AJAX or a different interface, there is also a
JSON API, although it is not in use in this example.

Considerations
--------------
I have removed the unit tests because they were written for the data I had put
in the database initially and would fail otherwise. In a more serious project
this should be expanded.

The JSON API is not as complete as the HTML one. Since it isn't used, I've only
included part of the API as a suggestion for what it should look like. If it
was to be used for real, it should not be too difficult to expand it.


Usage
=====
```
pip install -r requirements.txt

cd src && python3 server.py
```
