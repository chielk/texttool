import tornado.ioloop
import tornado.web
import tornado.template
from sqlite3 import OperationalError

from database import Database

database = Database()
try:
    database.create()  # Create the table if it doesn't exist yet
except OperationalError:
    pass

loader = tornado.template.Loader("html")
index = loader.load("index.html")
text_form = loader.load("text_form.html")
update_text_form = loader.load("update_text_form.html")


class MainHandler(tornado.web.RequestHandler):
    """Page with only the table of contents."""
    def get(self):
        self.write(index.generate(title="TextTool",
                                  body="",
                                  nav=database.get_list().html))


class ListHandlerJSON(tornado.web.RequestHandler):
    def get(self):
        self.write(database.get_list().json)


class TextHandler(tornado.web.RequestHandler):
    def get(self, text_id):
        text_obj = database.get_text(text_id)
        title = "TextTool - {title}".format(title=text_obj.title)
        self.write(index.generate(title=title,
                                  nav=database.get_list().html,
                                  body=text_obj.html))


    def post(self, text_id):
        """Update text."""
        title = self.get_argument('title')
        text = self.get_argument('text')
        database.update_title(text_id, title)
        database.update_text(text_id, text)

        text_obj = database.get_text(text_id)
        title = "TextTool - {title}".format(title=text_obj.title)
        self.write(index.generate(title=title,
                                  nav=database.get_list().html,
                                  body=text_obj.html))


class TextHandlerJSON(tornado.web.RequestHandler):
    def get(self, text_id):
        self.write(database.get_text(text_id).json)


class SearchHandler(tornado.web.RequestHandler):
    def get(self):
        substring = self.get_argument("q")
        title = "TextTool - \"{substring}\"".format(substring=substring)
        self.write(index.generate(title=title,
                                  nav=database.get_list().html,
                                  body=database.search_text(substring).html))


class SearchHandlerJSON(tornado.web.RequestHandler):
    def get(self):
        substring = self.get_argument("q")
        self.write(database.search_text(substring).json)


class NewTextHandler(tornado.web.RequestHandler):
    def post(self):
        title = self.get_argument('title')
        text = self.get_argument('text')
        text_id = database.add_text(title=title, text=text)
        text_obj = database.get_text(text_id)
        self.write(index.generate(title="TextTool - {}".format(title),
                                  nav=database.get_list().html,
                                  body=text_obj.html))

    def get(self):
        body = text_form.generate(title="", text="")
        self.write(index.generate(title="TextTool - new",
                                  nav=database.get_list().html,
                                  body=body))


class EditTextHandler(tornado.web.RequestHandler):
    def get(self, text_id):
        text_obj = database.get_text(text_id)
        html_title = "TextTool - {title}".format(title=text_obj.title)
        body = update_text_form.generate(text_id=text_id,
                                         title=text_obj.title,
                                         text=text_obj.text)
        self.write(index.generate(title=html_title,
                                  nav=database.get_list().html,
                                  body=body))


application = tornado.web.Application([
    # HTML
    tornado.web.URLSpec(r"/", MainHandler),
    tornado.web.URLSpec(r"/text/([0-9]+)", TextHandler, name='text_id'),
    tornado.web.URLSpec(r"/edit/text/([0-9]+)", EditTextHandler,
                        name='text_id'),
    tornado.web.URLSpec(r"/search", SearchHandler),
    tornado.web.URLSpec(r"/new", NewTextHandler),

    # JSON
    tornado.web.URLSpec(r"/api/list", ListHandlerJSON),
    tornado.web.URLSpec(r"/api/text/([0-9]+)", TextHandlerJSON,
                        name='text_id'),
    tornado.web.URLSpec(r"/api/search", SearchHandlerJSON),

    # Static
    (r'/static/(.*)', tornado.web.StaticFileHandler,
     {'path': "../static/"}),
])


if __name__ == "__main__":
    application.listen(8888)
    print("running server on http://localhost:8888")
    tornado.ioloop.IOLoop.current().start()
