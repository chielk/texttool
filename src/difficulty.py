"""
This provides a simple estimate of the difficulty of a piece of text.
More complicated and precise functions could use the prevalence of words taken
as an average over a body of works, or even the prevalence of words in texts
written for specific audiences (e.g. scientists or young children) to estimate
the difficulty.
"""
import re
import math

TEXT_LEN_DC = 100  # A length of this value scores half of the maximum

MIN_WORD_LEN = 3   # Consider 3 the minimum average word length difficulty
WORD_LEN_DC = 2    # With 2, 5 scores half of the maximum


def difficulty(text):
    """Compute the difficulty of a text as a function of length of text,
    length of words, number of different words.

    Half of the grade is based on the length of the text and half is based on
    the average length of unique words.

    Uses the asymptotic arctan function for both values.
    """

    # Remove punctuation and capitals
    text = re.sub(r'[^\w\s]', '', text.lower())
    words = text.split()
    unique = set(words)

    # Compute the average length of each unique word
    total_length = 0.
    total_count = 0.
    for word in unique:
        total_length += len(word)
        total_count += 1
    avg = total_length / max(1, total_count)

    avg_len_score = (9 * math.atan(max(0, avg - MIN_WORD_LEN) / WORD_LEN_DC) /
                     math.pi)
    text_len_score = 9 * math.atan(len(words) / TEXT_LEN_DC) / math.pi

    return 1 + avg_len_score + text_len_score
