import sqlite3
import datetime
import json

from difficulty import difficulty


class ContentList(object):
    """Wrapper class for list of the texts

    Stores the text_ids, titles, and dates.
    """
    def __init__(self, query_result):
        self.contentlist = []
        for text_id, title, date, text in query_result:
            self.contentlist.append([text_id,
                                     title,
                                     date,
                                     round(difficulty(text))])

    @property
    def json(self):
        data = {'head': ['text_id', 'title', 'date', 'difficulty'],
                'content': self.contentlist}
        return json.dumps(data)

    @property
    def html(self):
        table = """
        <table class="table table-sm">
            <tr>
                <th>Title</th>
                <th>Difficulty</th>
                <th>Date</th>
            </tr>
            {}
        </table>
        """
        row = """
        <tr>
            <td><a href="/text/{text_id}">{title}</a></td>
            <td>{dc}
            <td>{date}</td>
        </tr>
        """
        rows = []
        for text_id, title, date, dc in self.contentlist:
            rows.append(row.format(text_id=text_id,
                                   title=title,
                                   date=date,
                                   dc=dc))
        return table.format("".join(rows))


class Text(object):
    """Wrapper class for text

    Stores the title, text, date, and text_id.
    """
    def __init__(self, query_result, text_id):
        self.title, self.text, self.date = query_result
        self.text_id = text_id
        self.dc = difficulty(self.text)

    @property
    def json(self):
        data = {'head': ['title', 'text', 'date', 'dc'],
                'content': [self.title,
                            self.text,
                            self.date,
                            round(self.dc)]}
        return json.dumps(data)

    @property
    def html(self):
        # TODO Maybe add a similar method with <form>
        result = """
        <div class="jumbotron">
        <h2>{title}</h2>
        <span class="small">difficulty: {dc}</span><br />
        <span class="small text-muted">{date}</span>
        <a class="float-right" href="/edit/text/{text_id}">edit</a>
        <p>
          {text}
        </p>
        """
        return result.format(title=self.title,
                             date=self.date,
                             dc=round(self.dc),
                             text_id=self.text_id,
                             text=self.text)


class Database(object):
    """Database wrapper class

    Creates new cursor for each action to guarantee thread-safety.
    """
    def __init__(self, filename="text.db"):
        self.conn = sqlite3.connect(filename)

    def __del__(self):
        self.conn.close()

    def create(self):
        """Create the database

        raises an OperationalError if the table already exists.
        """
        query = "CREATE TABLE Text ( Title text, Text text, date text )"
        cur = self.conn.cursor()
        cur.execute(query)
        self.conn.commit()

    def get_list(self):
        """Get the list of texts

        returns an iterator over tuples - (text_id, title, date difficulty)
        """
        query = "SELECT rowid, title, date, text FROM Text"
        cur = self.conn.cursor()
        result = cur.execute(query)
        self.conn.commit()
        return ContentList(result)

    def get_text(self, text_id):
        """Get a text from the database

        returns a tuple - (title, text, date)
        """
        query = "SELECT * FROM Text WHERE rowid = ?"
        cur = self.conn.cursor()
        result = cur.execute(query, (text_id,)).__next__()
        self.conn.commit()
        return Text(result, text_id)

    def add_text(self, title, text):
        """Add text to the database

        returns text_id
        """
        query = "INSERT INTO Text VALUES (?, ?, ?)"
        # Remove fractions of seconds:
        date_str = str(datetime.datetime.now()).split(".")[0]
        cur = self.conn.cursor()
        cur.execute(query, (title, text, date_str))
        text_id = cur.lastrowid
        self.conn.commit()
        return text_id

    def search_text(self, substring):
        query = "SELECT rowid, title, date, text FROM Text WHERE text LIKE ?"
        cur = self.conn.cursor()
        # % are to remove ^ and $; tuple is so that it does not try to loop
        # over the letters
        result = cur.execute(query, ('%' + substring + '%',))
        self.conn.commit()
        return ContentList(result)

    def update_text(self, text_id, text):
        query = "UPDATE Text SET text = ? WHERE rowid = ?"
        cur = self.conn.cursor()
        cur.execute(query, (text, text_id))
        self.conn.commit()

    def update_title(self, text_id, title):
        query = "UPDATE Text SET title = ? WHERE rowid = ?"
        cur = self.conn.cursor()
        cur.execute(query, (title, text_id))
        self.conn.commit()
